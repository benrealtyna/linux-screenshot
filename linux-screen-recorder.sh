# Description:
#     This bash take screenshots from specific screen area every one minute.
#
# Requirenments :
# 1- install scrot to take screenshots:
# 	> sudo apt-get install scrot
#
# 2- install imagemagick to crop special area of screenshots:
# 	> sudo apt-get install imagemagick   
#
# 3- Install Gnome shell extensions. (Note: It just need for add icon to the top bar.)
# 	> sudo apt install gnome-shell-extensions

DELAY=60;

#Create ~/Realtyna directory
DIR="${HOME}/Realtyna"
if [ ! -d "$DIR" ]; then
  mkdir "$DIR"
  echo "${DIR} directory created."
fi

#Create ~/Realtyna/Screenshots directory
DIR="${HOME}/Realtyna/Screenshots"
if [ ! -d "$DIR" ]; then
  mkdir "$DIR"
  echo "${DIR} directory created."
fi

#Create ~/Realtyna/yyyy-mm directory "Mounth directory"
MONTH_DATE=$(date +%Y-%m)
DIR="${HOME}/Realtyna/Screenshots/""$MONTH_DATE"
if [ ! -d "$DIR" ]; then
  mkdir "$DIR"
  echo "${DIR} directory created."
fi



#Create ~/Realtyna/Screenshots/yyyy-mm-dd directory with current date name
TODAY_DATE=$(date +%Y-%m-%d)
DIR="${HOME}/Realtyna/Screenshots/""$MONTH_DATE""/""$TODAY_DATE"
if [ ! -d "$DIR" ]; then
  mkdir "$DIR"
fi

echo " 📸 📂 Currect directory is: $DIR"
echo " 📸 🔴 Capturing screenshots ..."


# Add 📸 🔴 Icons to the top bar.
gdbus call --session --dest org.gnome.Shell --object-path /org/gnome/Shell --method org.gnome.Shell.Eval "var userName = imports.gi.GLib.get_user_name(); Main.panel._centerBox.add_child(new imports.gi.St.Label ({ text: '📸 🔴' , y_align: imports.gi.Clutter.ActorAlign.CENTER }))"
# TODO: Must remove 📸 🔴 from top bar whene terminate the bash.

while true;
do
	# Start process
	BEFOR_RUN=$(date '+%s');
	
	FILE_NAME=$(date '+%Y-%m-%d_%H-%M-%S.jpg');
	FILE_PATH=$DIR/$FILE_NAME;

	# take screenshot
	scrot $FILE_PATH

	# crop image to special size	
	# Use widthxheight+left+top to specify area.
	convert $FILE_PATH -crop 2560x1080+1920+120 $FILE_PATH
	convert  -resize 80% $FILE_PATH $FILE_PATH

	# End process
	AFTER_RUN=$(date '+%s');
	
	PROCESS_DURATION=$((AFTER_RUN-BEFOR_RUN))
	SLEEP_TIME=$((DELAY-PROCESS_DURATION))
	sleep $SLEEP_TIME
done;

