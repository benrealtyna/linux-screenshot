# Description:
---
This bash take screenshots from specific screen area every one minute.

# Requirenments:
---
1. install scrot to take screenshots:
` sudo apt-get install scrot`
2. install imagemagick to crop special area of screenshots:
`sudo apt-get install imagemagick`
3. Install Gnome shell extensions. (Note: It just need for add icon to the top bar.)
`sudo apt install gnome-shell-extensions`

# Run:
---
1. Copy the file in the home directory.

2. Run below command in terminal:
```
bash linux-screen-recorder.sh
```
# Define Specific area of screen to take screenshots:
---
## If you have one monitor, comment below line in the while loop.
```
	convert $FILE_PATH -crop 2560x1080+1920+120 $FILE_PATH
```
## If you have multiple monitor:
1. comment below lines in the while loop.


```
	convert $FILE_PATH -crop 2560x1080+1920+120 $FILE_PATH
	convert  -resize 80% $FILE_PATH $FILE_PATH
```

2. run the bash file and stop it.
3. Go to the "HOME/Realtyna/Screenshots/yyyy-mm-dd" and find your firs screenshot.
3. With help of a image editor find the top,left,width and height of your specific area

```
A sample of first screenshot image from two monitor.
If you want to make screenshots from monitor-2, you can get the left,top,width and height values like this sample.
		 			   
	    0              left
	 0 ─┼───────────────┼───────────────────────┐
		│	m			│						│
		│	o			│						│
		│	n			│						│
		│	i			│						│
  top ..│...t...........│_ _ _ _ _ _ _ _ _ _ _ _│
		│	o			│  <------width------>  │
		│	r			│ |						│
		│	#1			│ |height				│
		│				│ |		monitor-#2		│
		└───────────────┴───────────────────────┘
```
4. Uncomment bellow lines and replace your width,height,left and top values insted old numbers:

```
	convert $FILE_PATH -crop widthxheight+left+top $FILE_PATH
	convert  -resize 80% $FILE_PATH $FILE_PATH
```
5. save the file and use it.
